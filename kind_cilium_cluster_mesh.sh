#Cilium cluster mesh with kind

cat <<EOF | kind create cluster --image kindest/node:v1.28.0 --config -
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: cluster-01
nodes:
  - role: control-plane
  - role: worker
networking:
  disableDefaultCNI: true
  kubeProxyMode: none
  serviceSubnet: "10.11.0.0/16"
  podSubnet: "10.10.0.0/16"
EOF

cat <<EOF | kind create cluster --image kindest/node:v1.28.0 --config -
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: cluster-02
nodes:
  - role: control-plane
  - role: worker
networking:
  disableDefaultCNI: true
  kubeProxyMode: none
  serviceSubnet: "10.21.0.0/16"
  podSubnet: "10.20.0.0/16"
EOF


export CLUSTER1=kind-cluster-01
export CLUSTER2=kind-cluster-02

cilium install --context $CLUSTER1 --set cluster.name=cluster1 --set cluster.id=1 --set ipam.mode=kubernetes

cilium status --context $CLUSTER1 --wait
kubectl --context=$CLUSTER1 get secret -n kube-system cilium-ca -o yaml | \
  kubectl --context $CLUSTER2 create -f -
cilium install --context $CLUSTER2 --set cluster.name=cluster2 --set cluster.id=2 --set ipam.mode=kubernetes

cilium status --context $CLUSTER2 --wait

cilium clustermesh enable --service-type NodePort --context $CLUSTER1
cilium clustermesh enable --service-type NodePort --context $CLUSTER2

cilium status --context $CLUSTER1
cilium status --context $CLUSTER2

cilium clustermesh connect --context $CLUSTER1 --destination-context $CLUSTER2

#Demo
# from samples/helloworld from istio repo
#use ./gen-helloworld.sh --version cluster1 > helloworld-cluster1.yaml
#use ./gen-helloworld.sh --version cluster2 > helloworld-cluster2.yaml

kubectl --context $CLUSTER1 apply -f helloworld-cluster1.yaml
kubectl --context $CLUSTER2 apply -f helloworld-cluster2.yaml
kubectl --context $CLUSTER1 scale deploy helloworld-cluster1 --replicas=3
kubectl --context $CLUSTER2 scale deploy helloworld-cluster2 --replicas=3

kubectl --context $CLUSTER1 annotate service helloworld service.cilium.io/global=true
kubectl --context $CLUSTER2 annotate service helloworld service.cilium.io/global=true

for run in {1..100}; do curl helloworld:5000/hello; done